import sys
from pathlib import Path

src_path = Path(__file__).parent.parent.resolve()
sys.path.append(str(src_path))

import argparse
import json

import matplotlib.pyplot as plt
import pandas as pd
from dvclive import Live
from joblib import load
import seaborn as sns
from sklearn.metrics import f1_score, confusion_matrix, roc_auc_score, RocCurveDisplay
from utils.load_params import load_params





def eval(models_dir, 
         model_fname, 
         data_dir,
         results_eval_dir):
    
    X_test = pd.read_csv(data_dir/'X_test.csv')
    y_test = pd.read_csv(data_dir/'y_test.csv')
    model = load(models_dir/model_fname)
    predictions = model.predict_proba(X_test)
    y_prob = predictions[:, 1]
    y_pred = (y_prob >= 0.5).astype(int)
    
    with Live(dir=results_eval_dir, dvcyaml=True) as live:

        # Calculate and Log metrics
        f1 = f1_score(y_test, y_pred)
        roc_auc = roc_auc_score(y_test, y_prob)
        live.log_metric('f1', f1)
        live.log_metric('roc_auc', roc_auc)

        # Log Matplotlib Figure as an image
        svc_disp = RocCurveDisplay.from_estimator(model, X_test, y_test)
        live.log_image("roc.png", svc_disp.figure_)
        
        # Log Confusion Matrix 
        cm = confusion_matrix(y_test, y_pred, normalize='true') 
        sns.heatmap(cm, annot=True, cmap=plt.cm.Blues) 
        plt.savefig(results_eval_dir / 'cm.png')
        live.log_image('cm.png', results_eval_dir / 'cm.png')
        
        # Log Sklearn plots
        live.log_sklearn_plot("roc", y_test, y_prob)
        live.log_sklearn_plot("confusion_matrix", y_test, y_pred, name="cm.json")


if __name__ == '__main__':
    
    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('--config', dest='config', required=True)
    args = args_parser.parse_args()
    
    params = load_params(params_path=args.config)
   
    data_dir = Path(params.base.data_dir)
    models_dir = Path(params.train.models_dir)
    results_eval_dir = Path(params.base.results_eval_dir)
    model_fname = params.train.model_fname

    eval(models_dir=models_dir, 
         model_fname=model_fname, 
         data_dir=data_dir,
         results_eval_dir=results_eval_dir)
